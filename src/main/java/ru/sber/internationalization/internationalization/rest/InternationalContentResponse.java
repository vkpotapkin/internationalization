package ru.sber.internationalization.internationalization.rest;

import java.util.Date;

public class InternationalContentResponse {

    //С типами надо быть внимательным, читай памятку предупреждение
    String text;
    Double noFormatAmount;
    Date noFormatDate;
    String amount;
    String dateShort;
    String dateMedium;
    String dateLong;
    String dateFull;
    String timeShort;
    String timeMedium;
    String timeLong;
    String timeFull;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getNoFormatAmount() {
        return noFormatAmount;
    }

    public void setNoFormatAmount(Double noFormatAmount) {
        this.noFormatAmount = noFormatAmount;
    }

    public Date getNoFormatDate() {
        return noFormatDate;
    }

    public void setNoFormatDate(Date noFormatDate) {
        this.noFormatDate = noFormatDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDateShort() {
        return dateShort;
    }

    public void setDateShort(String dateShort) {
        this.dateShort = dateShort;
    }

    public String getDateMedium() {
        return dateMedium;
    }

    public void setDateMedium(String dateMedium) {
        this.dateMedium = dateMedium;
    }

    public String getDateLong() {
        return dateLong;
    }

    public void setDateLong(String dateLong) {
        this.dateLong = dateLong;
    }

    public String getDateFull() {
        return dateFull;
    }

    public void setDateFull(String dateFull) {
        this.dateFull = dateFull;
    }

    public String getTimeShort() {
        return timeShort;
    }

    public void setTimeShort(String timeShort) {
        this.timeShort = timeShort;
    }

    public String getTimeMedium() {
        return timeMedium;
    }

    public void setTimeMedium(String timeMedium) {
        this.timeMedium = timeMedium;
    }

    public String getTimeLong() {
        return timeLong;
    }

    public void setTimeLong(String timeLong) {
        this.timeLong = timeLong;
    }

    public String getTimeFull() {
        return timeFull;
    }

    public void setTimeFull(String timeFull) {
        this.timeFull = timeFull;
    }
}
