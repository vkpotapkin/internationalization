package ru.sber.internationalization.internationalization.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Component
public class LocaleFilter extends OncePerRequestFilter {

    InheritableThreadLocal<Locale> currentLocale;

    @Autowired
    @Qualifier("currentLocale")
    public void setCurrentLocale(InheritableThreadLocal<Locale> currentLocale) {
        this.currentLocale = currentLocale;
    }

    InheritableThreadLocal<String> currentAcceptLanguage;

    @Autowired
    @Qualifier("currentAcceptLanguage")
    public void setCurrentAcceptLanguage(InheritableThreadLocal<String> currentAcceptLanguage) {
        this.currentAcceptLanguage = currentAcceptLanguage;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String acceptLanguage = request.getHeader("Accept-Language");
        Locale preferredLocale = null;

        Cookie[] cookies = request.getCookies();
        if(cookies!=null){
            Cookie preferredLocaleCookie = Arrays.stream(cookies).filter((c)->c.getName().equals("preferredlocale")).findFirst().orElse(null);
            if (preferredLocaleCookie!=null){
                preferredLocale = Locale.forLanguageTag(preferredLocaleCookie.getValue());
            }
        }

        if(preferredLocale!=null){
            acceptLanguage = preferredLocale.toLanguageTag();
        }

        final List<Locale> acceptedLocales = new ArrayList<>();
        if (acceptLanguage != null) {
            final List<Locale.LanguageRange> ranges = Locale.LanguageRange.parse(acceptLanguage);

            if (ranges != null) {
                ranges.forEach(languageRange -> {
                    final String localeString = languageRange.getRange();
                    final Locale locale = Locale.forLanguageTag(localeString);
                    acceptedLocales.add(locale);
                });
            }
        }
        //Алгоритм выбора нужной локали из списка. Тут простой алгоритм, но каждый может реализовать чтото свое, например учитывать поддерживаемые приложением языки
        if(acceptedLocales !=null && acceptedLocales.size()>0){
            preferredLocale = acceptedLocales.get(0);
        }

        currentLocale.set(preferredLocale);
        currentAcceptLanguage.set(acceptLanguage);

        filterChain.doFilter(request, response);
    }
}

