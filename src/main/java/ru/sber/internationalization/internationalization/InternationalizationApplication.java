package ru.sber.internationalization.internationalization;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.*;

@SpringBootApplication
public class InternationalizationApplication {

	@Bean(name="messageSource")
	public ResourceBundleMessageSource bundleMessageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("locales/messages");
		messageSource.setDefaultEncoding("UTF-8");
		messageSource.setAlwaysUseMessageFormat(true);
		messageSource.setCacheSeconds(1);
		messageSource.setFallbackToSystemLocale(false);
		return messageSource;
	}

	//Хранилище выбранного пользователем языка. В прототипе это Map в памяти, а в целевом виде это должно быть персистентное хранилище
	@Bean(name = "storageLocale")
	public HashMap<String,String> storageLocale() {
		return new HashMap<String,String>();
	}


	//Хранилищи текущей локали в формате Locale
	@Bean(name = "currentLocale")
	public InheritableThreadLocal<Locale> currentLocale() {
		InheritableThreadLocal<Locale> currentLocaleContainer = new InheritableThreadLocal<>();
		return currentLocaleContainer;
	}

	//Хранилище текузего http заголовка Accept-Language, с весами например: zh-TW,zh;q=0.9,ru-RU;q=0.8,ru;q=0.7,en-US;q=0.6,en;q=0.5
	@Bean(name = "currentAcceptLanguage")
	public InheritableThreadLocal<String> currentAcceptLanguage() {
		InheritableThreadLocal<String> acceptLanguageContainer = new InheritableThreadLocal<>();
		return acceptLanguageContainer;
	}


//Раскоментировать если нужно посмотреть заголовок preferredlocale для kafka
//Установить кафку https://kafka.apache.org/quickstart
//Запуск ZooKiper bin/zookeeper-server-start.sh config/zookeeper.properties
//Запруск Kafka bin/kafka-server-start.sh config/server.properties
//Создать топик bin/kafka-topics.sh --describe --topic topic1 --bootstrap-server localhost:9092
//Консоль отправки сообщений bin/kafka-console-producer.sh --topic topic1 --bootstrap-server localhost:9092
/*
	@Bean
	public NewTopic topic() {
		return TopicBuilder.name("topic1")
				.partitions(10)
				.replicas(1)
				.build();
	}

	@KafkaListener(id = "myId", topics = "topic1")
	public void listen(
			@Payload String message,
			@Headers MessageHeaders messageHeaders)
	{
		System.out.println("Payload: " + message);
		System.out.println("Headers:" + messageHeaders);
		System.out.println("Header preferredlocale:" + messageHeaders.get("preferredlocale"));
	}*/

	public static void main(String[] args) {
		SpringApplication.run(InternationalizationApplication.class, args);
	}
}