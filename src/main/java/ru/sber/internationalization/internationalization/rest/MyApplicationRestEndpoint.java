package ru.sber.internationalization.internationalization.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;
import ru.sber.internationalization.internationalization.InternationalContentResponseBuilder;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.*;

@RestController
@EnableAutoConfiguration
public class MyApplicationRestEndpoint {

    MessageSource messageSource;
    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    //Хранилище выбранного пользователем языка. В прототипе это Map в памяти, а в целевом виде это должно быть персистентное хранилище
    Map<String,String> storageLocale;
    @Autowired
    @Qualifier("storageLocale")
    public void setLanguageHm(Map<String, String> storageLocale) {
        this.storageLocale = storageLocale;
    }

    //Хранилище текущей локали
    InheritableThreadLocal<Locale> currentLocale;
    @Autowired
    @Qualifier("currentLocale")
    public void setCurrentLocale(InheritableThreadLocal<Locale> currentLocale) {
        this.currentLocale = currentLocale;
    }

    InheritableThreadLocal<String> currentAcceptLanguage;

    @Autowired
    @Qualifier("currentAcceptLanguage")
    public void setCurrentAcceptLanguage(InheritableThreadLocal<String> currentAcceptLanguage) {
        this.currentAcceptLanguage = currentAcceptLanguage;
    }

    @Autowired
    private KafkaTemplate<Object, Object> kafkaTemplate;

    @PostMapping(value = "/getInternationalContent")
    @ResponseBody
    public InternationalContentResponse getInternationalContent (@RequestBody InternationalContentRequest request){
        Locale locale = currentLocale.get();
        InternationalContentResponse internationalContentResponse = InternationalContentResponseBuilder.getInternationalContentResponse(messageSource, locale);
        return internationalContentResponse;
    }

    @PostMapping(value = "/savePreferredLocale")
    String savePreferredLocale(@RequestBody Map<String,String> allParams, HttpServletRequest req, HttpServletResponse resp) {
        String newLang = allParams.get("language");

        //Сохранить выбранный польщователем язык с учетом идентификатора пользователя.
        String userId = "1234567890";
        storageLocale.put(userId, newLang);
        Cookie cookieLang = new Cookie("preferredlocale", newLang);
        cookieLang.setMaxAge(2592000);
        resp.addCookie(cookieLang);
        return newLang;
    }

    @RequestMapping("/getPreferredLocale")
    String getPreferredLocale(HttpServletRequest req, HttpServletResponse resp) {
        String userId = "1234567890";
        String lang = storageLocale.get(userId);
        if(lang!=null) {
            Cookie cookieLang = new Cookie("preferredlocale", lang);
            cookieLang.setMaxAge(2592000);
            resp.addCookie(cookieLang);
        }
        return lang;
    }

    @PostMapping(value = "/sendToKafkaInternationalContent")
    @ResponseBody
    public void sendToKafkaInternationalContent (@RequestBody InternationalContentRequest request){
        String preferredlocale = currentAcceptLanguage.get();

        Message<String> message = MessageBuilder
                .withPayload(request.toString())
                .setHeader(KafkaHeaders.TOPIC, "topic1")
                .setHeader(KafkaHeaders.MESSAGE_KEY, "999")
                .setHeader(KafkaHeaders.PARTITION_ID, 0)
                .setHeader("preferredlocale", preferredlocale)
                .build();

        kafkaTemplate.send(message);
    }

    @PostMapping(value = "/httpClientRequest")
    @ResponseBody
    public String sendHttpClientRequest () throws IOException, InterruptedException {
        String preferredlocale = currentAcceptLanguage.get();

        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .connectTimeout(Duration.ofSeconds(10))
                .build();

        HttpRequest httpPostRequest = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.noBody())
                .uri(URI.create("http://localhost:8080/httpClientRequestHandler"))
                .setHeader("Accept-Language", preferredlocale)
                .build();
        HttpResponse<String> response = httpClient.send(httpPostRequest, HttpResponse.BodyHandlers.ofString());

        return response.body();
    }
}
