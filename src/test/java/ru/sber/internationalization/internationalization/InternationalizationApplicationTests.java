package ru.sber.internationalization.internationalization;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

@SpringBootTest
class InternationalizationApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void test() throws ParseException {
		SimpleDateFormat dff = (SimpleDateFormat) SimpleDateFormat.getDateInstance();
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(2021, 11, 03);

		Date d = gc.getTime();


		Locale locale = Locale.forLanguageTag("zh-yue-Hant-CN");
		DateFormat dfTime = DateFormat.getTimeInstance(DateFormat.SHORT,locale);
		DateFormat dfDate = DateFormat.getDateInstance(DateFormat.SHORT,locale);
		DateFormat dfTimeDate = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);

		Locale localeRus = Locale.forLanguageTag("ru-RU");
		Locale localeUSA = Locale.forLanguageTag("en-US");
		Locale localeCN = Locale.forLanguageTag("zh-CH");
		DateFormat dfDateShortRUS = DateFormat.getTimeInstance(DateFormat.SHORT,localeRus);
		DateFormat dfDateShortUSA = DateFormat.getTimeInstance(DateFormat.SHORT,localeUSA);
		DateFormat dfDateShortCN = DateFormat.getTimeInstance(DateFormat.SHORT,localeCN);

		String strDateShortRUS = dfDateShortRUS.format(d);
		String strDateShortUSA = dfDateShortUSA.format(d);
		String strDateShortCN = dfDateShortCN.format(d);


		System.out.println("strDateShortRUS:" + strDateShortRUS + " strDateShortUSA:"+strDateShortUSA + " strDateShortCN:" + strDateShortCN);

		String strDfTime = dfTime.format(d);
		String strDfDate = dfDate.format(d);
		String strDfTimeDate = dfTimeDate.format(d);

		System.out.println("Hello World!" + " strDfTime:" + strDfTime + " strDfDate:"+strDfDate + " strDfTimeDate:" + strDfTimeDate);
	}

}

//Hello World! strDfTime:下午7:47:38 [莫斯科標準時間] strDfDate:2021年7月27日 星期二 strDfTimeDate:2021年7月27日 星期二 下午7:47:38 [莫斯科標準時間]
//Hello World! strDfTime:下午7:50:03 strDfDate:2021年7月27日 strDfTimeDate:2021年7月27日 下午7:50:03
//Hello World! strDfTime:下午7:51 strDfDate:2021/7/27 strDfTimeDate:2021/7/27 下午7:51