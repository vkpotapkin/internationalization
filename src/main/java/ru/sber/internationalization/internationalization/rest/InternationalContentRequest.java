package ru.sber.internationalization.internationalization.rest;

import java.util.Date;

public class InternationalContentRequest {

    //С типами надо быть внимательным, читай памятку предупреждение
    String text;
    Date date;
    Double amount;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "InternationalContentRequest{" +
                "text='" + text + '\'' +
                ", date=" + date +
                ", amount=" + amount +
                '}';
    }
}
