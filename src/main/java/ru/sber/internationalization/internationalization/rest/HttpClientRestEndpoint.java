package ru.sber.internationalization.internationalization.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.internationalization.internationalization.InternationalContentResponseBuilder;
import java.util.Locale;

@RestController
@EnableAutoConfiguration
public class HttpClientRestEndpoint {

    MessageSource messageSource;
    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    //Хранилище текущей локали
    InheritableThreadLocal<Locale> currentLocale;
    @Autowired
    @Qualifier("currentLocale")
    public void setCurrentLocale(InheritableThreadLocal<Locale> currentLocale) {
        this.currentLocale = currentLocale;
    }

    @PostMapping(value = "/httpClientRequestHandler")
    @ResponseBody
    public InternationalContentResponse getInternationalContent (){
        Locale locale = currentLocale.get();
        InternationalContentResponse internationalContentResponse = InternationalContentResponseBuilder.getInternationalContentResponse(messageSource, locale);
        return internationalContentResponse;
    }
}
