package ru.sber.internationalization.internationalization;

import org.springframework.context.MessageSource;
import ru.sber.internationalization.internationalization.rest.InternationalContentResponse;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class InternationalContentResponseBuilder {

    public static InternationalContentResponse getInternationalContentResponse(MessageSource messageSource, Locale locale){
        InternationalContentResponse internationalContentResponse = new InternationalContentResponse();
        BigDecimal amount = new BigDecimal(1147.55);
        Date date = new Date();
        //BEGIN
        /** Памятка предупреждение:
         *  Внимание. Код ниже только для примера форматирования данных в соответствии с локалью.
         *  Использовать l10n в dto не рекомендуется.
         *
         *  Очень важно понимать, что форматированные данные в т.ч с применением локали предназначены только для отображения на клиенте
         *  Вы должны быть уверены что не произойдет модификации и использования в вычислениях отформатированных данных по пути к клиенту
         *  и клиент не отправит отформатированные данные обратно на сервер как входящие параметры для обработки сервером.
         *  Игнорирование данного предупреждения может привести к ошибкам.
         *
         *  Например:
         *  1)У даты типа java.util.Date свой исходный строковый формат "2021-09-04T09:17:43.463+03:00" в случае если мы передали клиенту для
         *  отображения строку даты где нарушили исходный формат и в дополнение потеряли часовой пояс, то в момент обработки таких
         *  данных на промежуточных узлах или отправки таких данных обратно на сервер для обработки возможны два типа ошибок
         *  а)Ошибка сериализации данных в тип java.util.Date б)Искажение времени и даты.
         *  2)У чисел с типом double свой исходный формат 1147.55 в случае если вы отправите на сервер строку отформатированную
         *  в немецком формате например 1.147,55 то вы получите ошибку сериализации данных для типа double. А что еще может быть хуже
         *  так это представление в немецком формате 1.147 где подразумевалось одна тысяча сто сорок семь, а после приведения к типу
         *  double мы получили одну целую сто сорок семь тысячных что сильно искажает информацию и если логика этой цифры
         *  это сумма в фунтах стерлингов, то вместо тысячи ста сорока семи фунтов у нас будет один фунт и четырнадцать пенсов.
         *  В обоих примерах последствия ощутимы.
         *
         *  Если необходимо отобразить данные в определенном формате и потом использовать эти данные для последующей обработки,
         *  то необходимо вместе с отформатированными данными отправлять клиенту и данные в исходном формате используемого вами типа данных,
         *  и/или информацию о типе данных, а в момент отправки данных с клиента и в момент получения их на сервере
         *  производить валидацию этих данных и приведение к необходимым типам без ошибок, потери и искажения информации.
         */
        List<Date> dates = Arrays.asList(date);
        List<Double> amounts = Arrays.asList(amount.doubleValue());
        String messageText = messageSource.getMessage("messageText",null, locale);
        String messageDate = messageSource.getMessage("messageDate",dates.toArray(), locale);
        String messageTime = messageSource.getMessage("messageTime",dates.toArray(), locale);
        String messageAmount = messageSource.getMessage("messageAmount",amounts.toArray(), locale);
        internationalContentResponse.setText(messageText + " " + messageDate + " " + messageTime + " " + messageAmount);

        internationalContentResponse.setNoFormatDate(date);
        internationalContentResponse.setNoFormatAmount(amount.doubleValue());

        NumberFormat nf = NumberFormat.getNumberInstance(locale);
        internationalContentResponse.setAmount(nf.format(amount.doubleValue()));

        DateFormat dfShortDate = DateFormat.getDateInstance(DateFormat.SHORT,locale);
        DateFormat dfMediumDate = DateFormat.getDateInstance(DateFormat.MEDIUM,locale);
        DateFormat dfLongDate = DateFormat.getDateInstance(DateFormat.LONG,locale);
        DateFormat dfFullDate = DateFormat.getDateInstance(DateFormat.FULL,locale);

        internationalContentResponse.setDateShort(dfShortDate.format(date));
        internationalContentResponse.setDateMedium(dfMediumDate.format(date));
        internationalContentResponse.setDateLong(dfLongDate.format(date));
        internationalContentResponse.setDateFull(dfFullDate.format(date));

        DateFormat dfShortTime = DateFormat.getTimeInstance(DateFormat.SHORT,locale);
        DateFormat dfMediumTime = DateFormat.getTimeInstance(DateFormat.MEDIUM,locale);
        DateFormat dfLongTime = DateFormat.getTimeInstance(DateFormat.LONG,locale);
        DateFormat dfFullTime = DateFormat.getTimeInstance(DateFormat.FULL,locale);

        internationalContentResponse.setTimeShort(dfShortTime.format(date));
        internationalContentResponse.setTimeMedium(dfMediumTime.format(date));
        internationalContentResponse.setTimeLong(dfLongTime.format(date));
        internationalContentResponse.setTimeFull(dfFullTime.format(date));
        //END
        return internationalContentResponse;
    }
}
